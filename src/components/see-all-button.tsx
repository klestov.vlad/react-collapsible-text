import React, { FC } from "react";
import styled from "styled-components";

export interface SeeAllTextButtonProps {
  isTextOpen: boolean;
  switchTextOpen: () => void;
  className?: string;
}

export const SeeAllButton: FC<SeeAllTextButtonProps> = ({
  switchTextOpen,
  isTextOpen,
  className,
}) => {
  return (
    <Root onClick={switchTextOpen} className={className}>
      <Text>{isTextOpen ? "Close" : "See all"}</Text>
      <ArrowWrapper $isTextOpen={isTextOpen}>
        <Arrow $isTextOpen={isTextOpen} />
      </ArrowWrapper>
    </Root>
  );
};

const Root = styled.div`
  padding: 4px 8px;
  border: none;
  display: flex;
  align-items: center;
  background: none;
  transition: all 0.3s ease;
  position: relative;
  border-radius: 8px;
  text-decoration: none;
  flex-shrink: 0;
  cursor: pointer;
`;

const Text = styled.span`
  transition: all 0.2s ease;
  color: black;
  margin-right: 6px;
  z-index: 1;

  &:focus,
  &:active {
    outline: none;
  }
  &:hover {
    color: grey;
    text-decoration: underline;
  }
`;

const ArrowWrapper = styled.div<{ $isTextOpen: boolean }>`
  transition: all 0.2s ease;
  transform: translateY(${({ $isTextOpen }) => ($isTextOpen ? 1.5 : -1.5)}px);
`;

const Arrow = styled.div<{ $isTextOpen: boolean }>`
  position: relative;
  width: 6px;
  height: 6px;
  z-index: 1;
  transform-origin: center;
  transition: all 0.3s ease;
  transform: rotate(${({ $isTextOpen }) => ($isTextOpen ? 135 : -45)}deg);

  &::before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 1.33px;
    height: 100%;
    background-color: blue;
  }
  &::after {
    content: "";
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 1.33px;
    background-color: blue;
  }
`;
