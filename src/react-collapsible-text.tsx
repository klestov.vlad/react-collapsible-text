import debounce from "lodash/debounce";
import { React, RefObject, createRef, useEffect, useState } from "react";
import styled from "styled-components";
import { SeeAllButton } from "./components/see-all-button";

interface CollapsibleTextProps {
  textContent?: string | null;
  countOfLines: number;
  children?: React.ReactNode;
  isTextOpenInitially?: boolean;
}
export const CollapsibleText = ({
  textContent,
  countOfLines,
  isTextOpenInitially = false,
}: CollapsibleTextProps) => {
  const containerRef = createRef<HTMLDivElement>();
  const [isSeeAllTextButtonVisible, setIsSeeAllTextButtonVisible] =
    useState(false);
  const [isTextOpen, setIsTextOpen] = useState(isTextOpenInitially);

  const switchTextOpen = () => {
    setIsTextOpen((prevIsTextOpen) => !prevIsTextOpen);
  };

  useEffect(() => {
    const hasClamping = (el: RefObject<HTMLDivElement>) => {
      if (!el.current) return false;
      const { clientHeight, scrollHeight } = el.current;
      return clientHeight !== scrollHeight;
    };

    const checkButtonAvailability = () => {
      if (containerRef.current) {
        setIsSeeAllTextButtonVisible(hasClamping(containerRef));
      }
    };

    const debouncedCheck = debounce(checkButtonAvailability, 300);

    checkButtonAvailability();
    window.addEventListener("resize", debouncedCheck);

    return () => {
      window.removeEventListener("resize", debouncedCheck);
    };
  }, [containerRef]);
  return (
    <Root>
      <Text
        $isTextOpen={isTextOpen}
        $countOfLines={countOfLines}
        ref={containerRef}
      >
        {textContent}
      </Text>
      <SeeAllTextButtonStyledContainer>
        {(isSeeAllTextButtonVisible || isTextOpen) && (
          <SeeAllButton
            isTextOpen={isTextOpen}
            switchTextOpen={switchTextOpen}
          />
        )}
      </SeeAllTextButtonStyledContainer>
    </Root>
  );
};

const Root = styled.div``;

const SeeAllTextButtonStyledContainer = styled.div`
  height: 28px;
  margin-top: 12px;
`;

const Text = styled.p<{
  $isTextOpen: boolean;
  $countOfLines: number;
  $typography?: string;
}>`
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: ${({ $isTextOpen, $countOfLines }) =>
    $isTextOpen ? "none" : $countOfLines};
  -webkit-box-orient: vertical;
  text-overflow: ellipsis;
  word-break: break-word;
  white-space: pre-line;
`;
